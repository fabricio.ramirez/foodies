import {Pagination} from '@entities/pagination';
import {Dish} from '@entities/dish';
import axiosInstance from '@service/axios';
import {AxiosError} from 'axios';

interface GetDishes {
  arg: {
    q?: string;
    page: number;
    limit?: number;
    categories?: string[];
  };
}

const getDishes = async (_: string, {arg}: GetDishes): Promise<Pagination<Dish>> => {
  try {
    const response = await axiosInstance.get('/dishes', {
      params: {
        page: arg.page,
        limit: arg.limit || 10,
        ...(arg.categories && {categories: arg.categories}),
        ...(arg.q && {q: arg.q}),
      },
    });

    return response.data;
  } catch (e) {
    const error = e as AxiosError;
    throw error;
  }
};

export default getDishes;

import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: `${process.env.NEXT_PUBLIC_API_URL}`,
  headers: {
    'Content-Type': 'application/json',
    accept: 'application/json',
  },
});

export default axiosInstance;

export const axiosFetch = axios.create({
  baseURL: `https://unshorten.me/s/`,
  headers: {
    'Content-Type': 'application/json',
    accept: 'application/json',
  },
});

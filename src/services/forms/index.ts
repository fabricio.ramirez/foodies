import axiosInstance from '@service/axios';
import {AxiosError} from 'axios';

interface SendForm {
  arg: {
    name: string;
    email: string;
    message: string;
  };
}
const sendForm = async (_: string, {arg}: SendForm) => {
  try {
    const response = await axiosInstance.post('/forms/contact/submissions', arg);
    return response.data;
  } catch (e) {
    const error = e as AxiosError;
    throw error;
  }
};

export default sendForm;

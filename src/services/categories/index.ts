import {NoPagination} from '@entities/pagination';
import {Category} from '@entities/dish';
import axiosInstance from '@service/axios';
import {AxiosError} from 'axios';

const getCategories = async (): Promise<NoPagination<Category>> => {
  try {
    const response = await axiosInstance.get('/categories', {});

    return response.data;
  } catch (e) {
    const error = e as AxiosError;
    throw error;
  }
};

export default getCategories;

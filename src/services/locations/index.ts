import {Pagination} from '@entities/pagination';
import {Location} from '@entities/location';
import axiosInstance, {axiosFetch} from '@service/axios';
import {AxiosError} from 'axios';

interface GetLocations {
  arg: {
    q?: string;
    page: number;
    limit?: number;
    type?: string;
  };
}

interface GetLocationDetail {
  arg: {
    url: string;
  };
}

export const getLocationDetail = async (_: string, {arg}: GetLocationDetail) => {
  try {
    const response = await axiosFetch.get(arg.url.replace('https://', ''));

    return response.data;
  } catch (e) {
    const error = e as AxiosError;
    throw error;
  }
};

export const getLocations = async (
  _: string,
  {arg}: GetLocations
): Promise<Pagination<Location>> => {
  try {
    const response = await axiosInstance.get('/locations', {
      params: {
        page: arg.page,
        limit: arg.limit || 10,
        ...(arg.type && {type: arg.type}),
        ...(arg.q && {q: arg.q}),
      },
    });

    return response.data;
  } catch (e) {
    const error = e as AxiosError;
    throw error;
  }
};

import React from 'react';
import getLangFile from '@utils/lang';
import {Navbar} from '@components/Navbar/Navbar';
import Footer from '@components/Footer/Footer';

interface Props {
  children: React.ReactNode;
}

const Layout = async ({children}: Props) => {
  const dictionary = await getLangFile('home');

  return (
    <main className="min-h-screen overflow-x-hidden">
      <Navbar dictionary={dictionary} />
      {children}
      <Footer dictionary={dictionary} />
    </main>
  );
};

export default Layout;

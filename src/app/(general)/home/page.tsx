import Image from 'next/image';
import React from 'react';
import cx from 'classnames';
import Link from 'next/link';
import getLangFile from '@utils/lang';
import Title from '@components/Title/Title';
import Bar from '@components/Icons/Bar';
import Description from '@components/Description/Description';
import ArrowRight from '@components/Icons/ArrowRight';
import MainHero from '@components/Icons/Heroes/Main';
import AboutIcon from '@components/Icons/AboutIcon';
import LeftTestimonial from '@components/Icons/Heroes/LeftTestimonial';
import Slider from '@components/Slider/Slider';
import RightTestimonial from '@components/Icons/Heroes/RightTestimonial';
import DoubleArrows from '@components/Icons/DoubleArrows';
import LocationView from '@components/Location/LocationView';
import ContactView from '@components/Form/ContactView';

export default async function Home() {
  const dictionary = await getLangFile('home');

  return (
    <>
      <div className="mx-auto flex min-h-[50vh] w-full max-w-[95vw] items-center px-4 md:min-h-[65vh] lg:min-h-screen lg:max-w-[90vw] ">
        <div className="grid h-auto gap-4 sm:grid-cols-2 sm:items-center sm:gap-4 xl:gap-20">
          <div className="order-2 mr-0 mt-[-40px] sm:order-1 sm:pt-28 md:mr-0 lg:order-1 lg:mr-10 lg:pt-8 2xl:mr-[158px]">
            <div className="relative">
              <Title
                name={dictionary.hero.title}
                className={cx(
                  'block text-[35px] leading-[35px] lg:text-[60px]',
                  'font-bold text-black dark:text-white sm:text-4xl lg:text-6xl lg:leading-tight',
                  'font-druk dark:text-black'
                )}
              />
              <Bar
                width={411}
                height={54}
                className={cx(
                  'text-color-[#FFD600] absolute',
                  'bottom-0',
                  'left-[-5px] z-[-10] md:left-[-5px] lg:left-[-15px]',
                  'max-w-[242px] md:max-w-[337px] lg:max-w-[411px]'
                )}
              />
            </div>
            <Description
              text={dictionary.hero.description}
              className="my-3 mr-[30px] text-[16px] leading-tight text-dark-300 md:mr-[80px] lg:mr-[120px] lg:text-[18px]"
            />
            <div className="mt-7 grid w-full gap-3 py-6 sm:inline-flex">
              <Link
                className="inline-flex items-center gap-x-3 text-center font-syne text-[22px] font-bold dark:text-white lg:justify-center"
                href="#restaurants"
              >
                {dictionary.hero.button}
                <ArrowRight width={24} height={24} className="ml-4 text-black dark:text-white" />
              </Link>
            </div>
            <MainHero
              className="absolute right-0 top-[-190px] max-w-[365px] sm:top-[-165px] sm:max-w-[340px] md:top-[-130px] md:max-w-[350px] lg:right-0 lg:top-[-10px] lg:max-w-[500px] xl:max-w-[792px]"
              width={792}
              height={794}
            />
          </div>
          <div className="relative order-1 flex justify-center pt-8 sm:order-2 sm:justify-end lg:order-2 lg:pb-20">
            <Image
              src="/img/hero_hamburger.png"
              width="567"
              height="446"
              className="z-20 max-w-[313px] rounded-md py-[85px] md:py-0 lg:py-0 xl:max-w-[567px]"
              alt="Image Description"
            />
          </div>
        </div>
      </div>
      <div id="about" className="mx-auto flex h-auto w-full scroll-m-[101px] items-center">
        <div className="grid h-auto w-full md:items-start lg:grid-cols-11">
          <div className="relative z-10 col-span-6 flex justify-center overflow-x-hidden md:pl-0 lg:justify-end lg:pl-0">
            <Image
              src="/img/hero_about.png"
              width="816"
              height="565"
              className="left-0 top-0 w-full"
              alt="Image Description"
            />
            <div className="absolute bottom-0 right-0 px-5 py-6 md:block md:px-14 md:py-14 lg:block lg:px-10 lg:py-16">
              <Title
                name={dictionary.about.image.title_1}
                className="block h-full py-0 text-right font-druk text-[35px] font-bold leading-[35px] text-white md:text-[50px] md:leading-[50px] lg:text-[50px] lg:leading-[50px]"
                name2={dictionary.about.image.title_2}
                className2="text-yellow-500"
              />
            </div>
          </div>
          <div className="relative col-span-5 flex h-full w-auto flex-col items-center justify-center py-[40px] sm:py-[40px] md:py-[120px] lg:py-[40px]">
            <div className="relative z-10 mx-4 md:mx-[80px] lg:mx-[40px] xl:mx-[80px]">
              <div className="w-full">
                <Title
                  name={dictionary.about.title}
                  className="block h-full font-syne text-[22px] font-bold text-gray-800 dark:text-white"
                />
              </div>
              <div className="w-full">
                <Description
                  text={dictionary.about.description}
                  className="mt-6 text-[18px] leading-[24.51px] text-dark-300"
                />
              </div>
              <div className="mt-6 grid w-full pt-4 sm:inline-flex">
                <Link
                  className="inline-flex items-center justify-start gap-x-3 text-center font-syne text-[22px] font-bold dark:text-white"
                  href="#contact"
                >
                  {dictionary.about.button}
                  <ArrowRight width={24} height={24} className="ml-4 dark:text-white" />
                </Link>
              </div>
            </div>
            <div className="z-1 md:max-w-screen absolute right-0 hidden sm:top-[20%] md:top-[10%] md:block lg:top-[20%] lg:block lg:w-full">
              <AboutIcon className="h-full w-full md:w-[1024px] md:max-w-[1024px] lg:max-h-[352px] lg:max-w-[830px] xl:max-h-[400px] xl:max-w-[890px]" />
            </div>
          </div>
        </div>
      </div>
      <div id="restaurants" className="w-full scroll-m-[101px]">
        <LocationView dictionary={dictionary.locations} />
      </div>
      <div className="relative w-full py-12 md:pb-24 md:pt-48 lg:pb-36 lg:pt-64">
        <div className="absolute left-[-5px] top-[-45px] lg:left-0 lg:top-[80px]">
          <LeftTestimonial className="hidden max-w-[150px] md:block lg:block lg:max-w-[187px]" />
        </div>
        <div className="flex flex-col items-center justify-center">
          <Slider testimonials={dictionary.testimonials} />
        </div>
        <div className="absolute bottom-[-110px] right-0 lg:bottom-[-60px] lg:right-0">
          <RightTestimonial className="hidden max-w-[160px] md:block lg:block lg:max-w-[214px]" />
        </div>
        <Image
          src="/img/ketchup_testimonial.png"
          width={464}
          height={763}
          alt="ketchup"
          className="absolute bottom-[-150px] right-0 hidden lg:-right-20 lg:block xl:right-0"
        />
      </div>
      <div id="contact" className="scroll-m-[101px] bg-black pb-[400px] pt-16 lg:py-24">
        <ContactView dictionary={dictionary.contact} />
      </div>
      <div className="relative flex w-full items-center justify-center overflow-y-visible pb-36 pt-24">
        <div className="flex flex-col lg:grid lg:grid-cols-4 xl:max-w-[90vw] 2xl:max-w-[70vw]">
          <div className="col-span-1 h-[150px] w-full md:h-[300px] lg:h-auto">
            <Image
              src="/img/mobile_download.png"
              width={971}
              height={1031}
              alt="mobile_download"
              className="absolute left-16 top-[-250px] max-w-[300px] sm:left-36 sm:top-[-280px] sm:max-w-[300px] md:left-40 md:top-[-310px] md:max-w-[450px] lg:left-4 lg:top-[-115px] lg:max-w-[500px] xl:left-4 xl:max-w-[500px] 2xl:left-24 2xl:max-w-[500px]"
            />
          </div>
          <div className="relative col-span-3 w-full md:max-w-[787px] lg:max-w-[787px]">
            <div className="mx-4 flex flex-col items-center justify-center md:mx-20 lg:mx-20">
              <div className="relative mb-16 w-full text-center">
                <Title
                  name={dictionary.app.title}
                  className={cx(
                    'text-center font-druk text-[30px] font-bold leading-[35px] text-black dark:text-white md:text-[35px] lg:text-[35px]'
                  )}
                />
                <Bar
                  width={558}
                  height={44}
                  className={cx(
                    'absolute bottom-6 left-4 z-[-1] block max-w-[558px] md:left-10 md:hidden lg:hidden'
                  )}
                />
                <Bar
                  width={558}
                  height={44}
                  className={cx(
                    'absolute bottom-[-8px] left-4 z-[-1] max-w-[558px] md:bottom-[-30px] md:left-6 lg:bottom-[-30px] lg:left-10'
                  )}
                />
              </div>
              <div className="w-full pb-20">
                <div className="flex flex-col gap-16 md:grid md:grid-cols-2 lg:grid lg:grid-cols-2">
                  <div className="col-span-1 flex flex-col items-center justify-start text-center">
                    <div className="w-full">
                      <span className="max-h-[36px] max-w-[36px] rounded-full bg-yellow-500 px-[7.65px] py-1.5 text-[18px] font-bold text-white">
                        01
                      </span>
                    </div>
                    <div className="w-full py-4">
                      <Title
                        name={dictionary.app.items.one.title}
                        className="text-[18px] font-bold dark:text-white"
                      />
                    </div>
                    <div className="w-full">
                      <Description
                        text={dictionary.app.items.one.description}
                        className="text-[18px] leading-[21px] dark:text-white"
                      />
                    </div>
                  </div>
                  <div className="col-span-1 flex flex-col items-center justify-start text-center">
                    <div className="w-full">
                      <span className="max-h-[36px] max-w-[36px] rounded-full bg-yellow-500 px-[7.65px] py-1.5 text-[18px] font-bold text-white">
                        02
                      </span>
                    </div>
                    <div className="w-full py-4">
                      <Title
                        name={dictionary.app.items.two.title}
                        className="text-[18px] font-bold dark:text-white"
                      />
                    </div>
                    <div className="w-full">
                      <Description
                        text={dictionary.app.items.two.description}
                        className="text-[18px] leading-[21px] dark:text-white"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div className="w-full md:max-w-[300px] lg:max-w-[300px]">
                <div className="flex flex-col items-center justify-start text-center md:mr-8 lg:mr-8">
                  <div className="w-full">
                    <span className="max-h-[36px] max-w-[36px] rounded-full bg-yellow-500 px-[7.65px] py-1.5 text-[18px] font-bold text-white">
                      03
                    </span>
                  </div>
                  <div className="w-full py-4">
                    <Title
                      name={dictionary.app.items.three.title}
                      className="text-[18px] font-bold dark:text-white"
                    />
                  </div>
                  <div className="w-full">
                    <Description
                      text={dictionary.app.items.three.description}
                      className="text-[18px] leading-[21px] dark:text-white"
                    />
                  </div>
                </div>
              </div>
            </div>
            <DoubleArrows
              width={524}
              height={248}
              className="absolute -z-20 hidden md:left-[127px] md:top-[138px] md:block md:max-w-[524px] lg:left-[137px] lg:top-[138px] lg:block lg:max-w-[524px]"
            />
          </div>
        </div>
      </div>
    </>
  );
}

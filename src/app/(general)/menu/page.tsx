import Image from 'next/image';
import React from 'react';
import cx from 'classnames';
import getLangFile from '@utils/lang';
import Title from '@components/Title/Title';
import Bar from '@components/Icons/Bar';
import MenuLarge from '@components/Icons/Heroes/MenuLarge';
import MenuMedium from '@components/Icons/Heroes/MenuMedium';
import MenuSmall from '@components/Icons/Heroes/MenuSmall';
import MenuView from '@components/Menu/MenuView';

export default async function Home() {
  const dictionary = await getLangFile('menu');

  return (
    <div>
      <div className="bg-black">
        <div className="mx-auto flex min-h-screen w-full max-w-[95vw] items-center px-4 md:min-h-[503px] lg:min-h-[610px] lg:max-w-[90vw] lg:items-end">
          <div className="grid h-auto gap-4 md:grid-cols-2 md:items-center md:gap-4 xl:gap-20">
            <div className="order-2 mr-0 mt-[-40px] md:order-1 md:mr-0 md:pt-8 lg:order-1 lg:mr-0 lg:pt-8 xl:mr-10">
              <div className="relative">
                <Title
                  name={dictionary.hero.title_1}
                  name2={dictionary.hero.title_2}
                  className2="text-black"
                  className={cx(
                    'block text-[33.95px] md:text-[40px]' +
                      'px] leading-[35px] md:leading-[40px] lg:text-[60px]',
                    'font-bold text-white sm:text-4xl lg:text-6xl lg:leading-[60px]',
                    'relative z-[10] font-druk dark:text-white'
                  )}
                />
                <Bar
                  width={711}
                  height={54}
                  className={cx(
                    'text-color-[#FFD600] absolute',
                    'top-[70px] md:top-[80px] lg:top-[180px] xl:top-[120px]',
                    'left-[-5px] z-[1] sm:top-[40px] md:left-[-5px] lg:left-[-15px]',
                    'max-w-[295px] sm:max-w-[310px] md:max-w-[537px] lg:w-[600px] xl:max-w-[540px]'
                  )}
                />
              </div>
              <MenuLarge className="absolute right-0 top-0 hidden lg:block" />
              <MenuMedium className="absolute right-0 top-0 hidden md:block lg:hidden" />
              <MenuSmall className="absolute right-0 top-0 block md:hidden lg:hidden" />
            </div>
            <div className="relative order-1 flex items-center justify-center py-2 md:order-2 lg:order-2">
              <Image
                src="/img/hero_hamburger_menu.png"
                width="628"
                height="472"
                className="z-10 max-w-[328px] rounded-md pb-[80px] pt-[-5px] md:max-w-[340px] md:py-0 lg:max-w-[628px] lg:py-0"
                alt="Image Description"
              />
            </div>
          </div>
        </div>
      </div>
      <MenuView />
    </div>
  );
}

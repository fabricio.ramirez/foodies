import './globals.css';
import type {Metadata} from 'next';
import ThemeProvider from './theme-provider';

export const metadata: Metadata = {
  title: 'Foodies',
  description: 'Un nuevo sabor esta en la ciudad',
};

export default function RootLayout({children}: {children: React.ReactNode}) {
  return (
    <html lang="es">
      <link rel="icon" href="/favicon.png" />
      <body className="bg-white dark:bg-dark-600">
        <ThemeProvider attribute="class" defaultTheme="system" enableSystem>
          {children}
        </ThemeProvider>
      </body>
    </html>
  );
}

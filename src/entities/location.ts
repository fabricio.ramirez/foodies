export interface Location {
  id: string;
  title: string;
  description: string;
  url: string;
  type: string;
  latitude?: number;
  longitude?: number;
}

export interface SelectedLocation {
  id: string;
  name: string;
  position: {
    lat: number;
    lng: number;
  };
}

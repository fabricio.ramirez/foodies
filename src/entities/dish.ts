export interface Dish {
  id: string;
  title: string;
  description: string;
  price: number;
  image: string;
  categories: Category[];
}

export interface Category {
  id: string;
  name: string;
  slug: string;
}

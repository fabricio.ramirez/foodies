import {
  createErrorObject,
  isEmailRefined,
  maxCharactersRefined,
  requiredRefined,
} from '@validators/utils';
import {useCallback} from 'react';
import {object, Infer, StructError, validate, intersection} from 'superstruct';

interface SchemaMessages {
  name: {
    required: string;
    max: string;
  };
  email: {
    required: string;
    max: string;
    invalid: string;
  };
  message: {
    required: string;
    max: string;
  };
}

const schema = (messages: SchemaMessages) =>
  object({
    name: intersection([
      requiredRefined(messages.name.required),
      maxCharactersRefined(messages.name.max, 50),
    ]),
    email: intersection([
      requiredRefined(messages.email.required),
      isEmailRefined(messages.email.invalid),
      maxCharactersRefined(messages.email.max, 50),
    ]),
    message: intersection([
      requiredRefined(messages.message.required),
      maxCharactersRefined(messages.message.max, 50),
    ]),
  });

export type SendFormData = Infer<ReturnType<typeof schema>>;

const resolver = (messages: SchemaMessages) => async (data: SendFormData) => {
  try {
    const values = validate(data, schema(messages));
    if (values[0]) {
      throw values[0];
    }
    return {values: values[1], errors: {}};
  } catch (e) {
    const error = e as StructError;
    console.log({
      values: {},
      errors: createErrorObject(error),
    });
    return {
      values: {},
      errors: createErrorObject(error),
    };
  }
};

export const useSendFormDataResolver = (messages: SchemaMessages) =>
  useCallback(() => resolver(messages), [messages]);

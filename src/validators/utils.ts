import {StructError, refine, string} from 'superstruct';

type ErrorObject = {
  [key: string]: {
    type: string;
    message: string;
  };
};

export const createErrorObject = (error: StructError) => {
  const failures = error.failures();
  const errors = failures.reduce<ErrorObject>((acc, failure) => {
    if (acc[failure.key]) {
      return acc;
    }
    return {
      ...acc,
      [failure.key]: {
        type: failure.type,
        message: failure.refinement,
      },
    };
  }, {});
  return errors;
};

const isEmail = (value: string) => {
  const regex = /^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$/;
  return regex.test(value);
};

const required = (value: string) => {
  if (!value) return false;
  return value.length > 0;
};

const maxCharacters = (value: string, max: number) => value.length <= max;

export const isStringRefined = (message: string) =>
  refine(string(), message, value => typeof value === 'string');

export const isEmailRefined = (message: string) => refine(string(), message, isEmail);

export const requiredRefined = (message: string) => refine(string(), message, required);

export const maxCharactersRefined = (message: string, max: number) =>
  refine(string(), message, value => maxCharacters(value, max));

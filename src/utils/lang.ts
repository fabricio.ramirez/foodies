import {cookies} from 'next/headers';
// @ts-ignore
import {Translations} from 'translations';
import getTranslation from '@components/Translation';

const getLangFile = async <T extends keyof Translations>(path: T) => {
  const userLang = cookies().get('lang')?.value || 'es';
  return (await getTranslation(path, userLang as 'es' | 'en')) as Translations[T];
};

export default getLangFile;

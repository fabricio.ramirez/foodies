'use client';

// @ts-ignore
import Swiper from 'swiper/bundle';
import 'swiper/css/bundle';
// @ts-ignore
import {Translations} from 'translations';
import cx from 'classnames';
import {useEffect} from 'react';

interface Props {
  testimonials: Translations['home']['testimonials'];
}

function Slider({testimonials}: Props) {
  useEffect(() => {
    const swiper = new Swiper('.swiper', {
      direction: 'horizontal',
      loop: true,
      autoHeight: false,
      centeredSlides: true,
      slidesPerView: 1,

      pagination: {
        el: '.swiper-pagination',
        type: 'fraction',
      },

      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
      },
    });

    console.log(swiper);
  }, []);

  return (
    <div className="swiper max-w-[90vw] md:max-w-[80vw] lg:max-w-[55vw]">
      <div className="swiper-wrapper">
        {testimonials.map((item: {title: string; description: string}) => (
          <div key={item.title} className="swiper-slide">
            <div className="content-wrapper">
              <div className="content flex flex-col items-center justify-center">
                <div className="w-full text-center">
                  <h1
                    className={cx(
                      'font-druk font-bold dark:text-white md:text-[30px] lg:text-[36px]',
                      'text-center text-[20px] leading-[25.16px] md:leading-[37.71px] lg:leading-[45.29px]'
                    )}
                  >
                    {item.title}
                  </h1>
                </div>
                <div className="w-full py-4 text-center lg:max-w-[35vw]">
                  <span className="text-center text-[18px] leading-[23.2px] text-dark-300">
                    {item.description}
                  </span>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
      <div className="swiper-nav-wrapper">
        <div className="swiper-button-prev" />
        <div className="swiper-pagination font-syne text-[20px] font-bold" />
        <div className="swiper-button-next" />
      </div>
    </div>
  );
}

export default Slider;

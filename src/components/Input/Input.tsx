import {Control, Controller, FieldPath, FieldValues} from 'react-hook-form';
import React from 'react';

export interface InputProps<T extends FieldValues> extends React.HTMLAttributes<HTMLInputElement> {
  type: string;
  labelClassName?: string;
  labelText?: string;
  placeholder: string;
  name: FieldPath<T>;
  control: Control<T>;
  disabled?: boolean;
  errorMessage?: string;
  required?: boolean;
  maxLength?: number;
}

function Input<T extends object>({
  type,
  labelClassName,
  labelText,
  placeholder,
  name,
  control,
  disabled,
  errorMessage,
  required,
  maxLength,
  className,
  ...props
}: InputProps<T>) {
  return (
    <div className="w-full">
      <div className="mb-3 text-dark-100 focus-within:text-yellow-500">
        <label htmlFor="name" className={labelClassName}>
          {labelText}
        </label>
        <Controller
          name={name}
          control={control}
          render={({field}) => (
            /* eslint-disable react/jsx-props-no-spreading */
            <input
              type={type}
              className={className}
              placeholder={placeholder}
              disabled={disabled}
              maxLength={maxLength}
              required={required}
              {...field}
              {...props}
              onChange={e => {
                field.onChange(e.target.value);
              }}
            />
          )}
        />
        {errorMessage && (
          <span
            id={`error-${name}`}
            className="text-alert-error-400 text-xs font-medium leading-5 tracking-[0.4px]"
          >
            {errorMessage}
          </span>
        )}
      </div>
    </div>
  );
}

const defaultProps = {
  labelClassName: '',
  labelText: '',
  disabled: false,
  errorMessage: '',
  required: false,
  maxLength: 225,
};

Input.defaultProps = defaultProps;

export default Input;

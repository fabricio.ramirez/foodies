import {Control, Controller, FieldPath, FieldValues} from 'react-hook-form';
import React from 'react';

interface Props<T extends FieldValues> extends React.HTMLAttributes<HTMLTextAreaElement> {
  labelClassName?: string;
  labelText?: string;
  disabled?: boolean;
  errorMessage?: string;
  placeholder: string;
  name: FieldPath<T>;
  control: Control<T>;
  required?: boolean;
  rows?: number;
}

function TextArea<T extends object>({
  labelClassName = 'text-dark-grey-200',
  labelText,
  className,
  disabled,
  errorMessage,
  placeholder,
  name,
  control,
  required,
  rows,
  ...props
}: Props<T>) {
  return (
    <div className="w-full">
      <div className="mb-3 text-dark-100 focus-within:text-yellow-500">
        <label htmlFor={name} className={labelClassName}>
          {labelText}
        </label>
        <Controller
          name={name}
          control={control}
          render={({field}) => (
            /* eslint-disable react/jsx-props-no-spreading */
            <textarea
              className={className}
              rows={rows}
              id={name}
              placeholder={placeholder}
              disabled={disabled}
              {...props}
              {...field}
            />
          )}
        />
        {errorMessage && (
          <span
            id={`error-${name}`}
            className="text-alert-error-400 text-xs font-medium leading-5 tracking-[0.4px]"
          >
            {errorMessage}
          </span>
        )}
      </div>
    </div>
  );
}

const defaultProps = {
  labelClassName: '',
  labelText: '',
  disabled: false,
  errorMessage: '',
  required: false,
  rows: 5,
};

TextArea.defaultProps = defaultProps;

export default TextArea;

'use client';

import {useLoadScript, GoogleMap, Marker, InfoWindow} from '@react-google-maps/api';
import {useMemo, useState} from 'react';
import {SelectedLocation} from '@entities/location';

interface Props {
  selectedLocation: SelectedLocation | null;
}

function GoogleMapView({selectedLocation}: Props) {
  const libraries = useMemo(() => ['places'], []);
  const mapCenter = useMemo(
    () => ({
      lat: selectedLocation?.position.lat ?? 13.704746,
      lng: selectedLocation?.position.lng ?? -89.2025575,
    }),
    [selectedLocation?.position.lat, selectedLocation?.position.lng]
  );

  const [activeMarker, setActiveMarker] = useState(null);
  const handleActiveMarker = (marker: any) => {
    if (marker === activeMarker) {
      return;
    }
    setActiveMarker(marker);
  };

  const mapOptions = useMemo<google.maps.MapOptions>(
    () => ({
      disableDefaultUI: true,
      clickableIcons: true,
      scrollwheel: false,
      zoom: 2,
    }),
    []
  );

  const {isLoaded, loadError} = useLoadScript({
    googleMapsApiKey: `${process.env.NEXT_PUBLIC_GOOGLE_MAPS_API_KEY}`,
    libraries: libraries as any,
  });

  if (!isLoaded) {
    return <p>Loading...</p>;
  }

  if (loadError) {
    return <p>Error</p>;
  }

  return (
    <div>
      {/* @ts-ignore */}
      <GoogleMap
        options={mapOptions}
        zoom={14}
        center={mapCenter}
        mapTypeId={google.maps.MapTypeId.ROADMAP}
        mapContainerStyle={{width: 'auto', height: '605px'}}
        onLoad={() => {}}
      >
        {selectedLocation ? (
          /* @ts-ignore */
          <Marker
            key={selectedLocation.id}
            position={selectedLocation.position}
            onClick={() => handleActiveMarker(selectedLocation.id)}
          >
            {activeMarker === selectedLocation.id ? (
              /* @ts-ignore */
              <InfoWindow onCloseClick={() => setActiveMarker(null)}>
                <div>{selectedLocation.name}</div>
              </InfoWindow>
            ) : null}
          </Marker>
        ) : null}
      </GoogleMap>
    </div>
  );
}

export default GoogleMapView;

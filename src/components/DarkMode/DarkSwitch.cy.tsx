import React from 'react';
import DarkSwitch from './DarkSwitch';
import ThemeProvider from '../../app/theme-provider';

const darkSwitchSelector = 'button[data-cy="darkSwitch"]';

describe('<DarkSwitch />', () => {
  beforeEach(() => {
    cy.mount(
      <ThemeProvider attribute="class" defaultTheme="system" enableSystem>
        <DarkSwitch />
      </ThemeProvider>
    );
  });

  it(`should set the html data-theme to light by default`, () => {
    cy.get('html').should('have.class', 'light');
  });

  it('should change the theme to dark when clicking', () => {
    cy.get(darkSwitchSelector).click();

    cy.get('html').should('have.class', 'dark');
  });
});

'use client';

import ContactIcon from '@components/Icons/ContactIcon';
import Title from '@components/Title/Title';
import cx from 'classnames';
import Description from '@components/Description/Description';
import Button from '@components/Button/Button';
import React, {useState} from 'react';
import {Translations} from 'translations';
import useMutation from 'swr/mutation';
import sendForm from '@service/forms';
import {useForm} from 'react-hook-form';
import {useSendFormDataResolver} from '@validators/contactForm';
import Input from '@components/Input/Input';
import TextArea from '@components/Input/TextArea';
import {redirect} from 'next/navigation';

interface Props {
  dictionary: Translations['home']['contact'];
}

type SendFormData = {
  name: string;
  email: string;
  message: string;
};

function ContactView({dictionary}: Props) {
  const resolver = useSendFormDataResolver({
    name: {
      required: dictionary.form.name.required,
      max: dictionary.form.name.max,
    },
    email: {
      required: dictionary.form.email.required,
      max: dictionary.form.email.max,
      invalid: dictionary.form.email.invalid,
    },
    message: {
      required: dictionary.form.message.required,
      max: dictionary.form.message.max,
    },
  });

  const [isSuccess, setIsSuccess] = useState(false);

  const {handleSubmit, control, formState} = useForm<SendFormData>({
    resolver: resolver(),
  });

  const {trigger: triggerSendForm, isMutating: isSendingLoading} = useMutation(
    'send-form',
    sendForm,
    {
      onSuccess: async data => {
        if (data.message) {
          setIsSuccess(true);
        }

        setTimeout(() => {
          setIsSuccess(false);
        }, 5000);
      },
    }
  );

  const onSendForm = async (data: SendFormData) => {
    console.log(data);
    await triggerSendForm({
      name: data.name,
      email: data.email,
      message: data.message,
    });
  };

  return (
    <>
      <div
        className={`${isSuccess ? 'block' : 'hidden'} flex flex-col items-center justify-center`}
        data-cy="contactView"
      >
        <div className="flex w-full flex-col items-center justify-center">
          <div className="flex flex-col items-center justify-center pb-12">
            <ContactIcon width={158} height={143} />
          </div>
          <div className="flex flex-col items-center justify-center">
            <div className="w-full  max-w-[50vw] text-center">
              <h1 className="text-center font-druk text-[36px] font-bold text-white">
                {dictionary.title_success}
              </h1>
            </div>
            <div className="w-full max-w-[44vw] py-4">
              <p className="text-center text-[24px] leading-[32.68px] text-white">
                {dictionary.description}
              </p>
            </div>
          </div>
        </div>
        <div className="flex w-full max-w-[65vw] items-center justify-center pt-6">
          <div className="flex items-center justify-center pt-6">
            <Button
              onClick={() => {
                redirect('/menu');
              }}
              name={dictionary.button_success}
            />
          </div>
        </div>
      </div>
      <div
        className={`${!isSuccess ? 'block' : 'hidden'} flex flex-col items-center justify-center`}
      >
        <div className="w-full">
          <div className="flex flex-col items-center justify-center">
            <div className="w-full text-center lg:max-w-[50vw]">
              <Title
                name={dictionary.title}
                className={cx(
                  'text-center font-druk text-[35px] font-bold leading-[35px] text-white md:text-[36px] lg:text-[36px]'
                )}
              />
            </div>
            <div className="w-full max-w-[80vw] py-4 md:max-w-[60vw] lg:max-w-[44vw]">
              <Description
                text={dictionary.description}
                className="text-center text-white md:text-[18px] md:leading-[24.51px] lg:text-[24px] lg:leading-[32.68px]"
              />
            </div>
          </div>
        </div>
        <div className="flex w-full items-center justify-center pt-6 lg:max-w-[65vw]">
          <form
            onSubmit={handleSubmit(onSendForm)}
            className="flex w-full flex-col items-center justify-center lg:block"
          >
            <div className="flex w-full max-w-[85vw] flex-col items-center lg:grid lg:grid-cols-3 lg:gap-[40px]">
              <div className="w-full lg:col-span-1">
                <Input
                  name="name"
                  type="text"
                  control={control}
                  className="block w-full rounded-md border border-dark-100 bg-transparent px-5 py-4 text-[16px] text-white placeholder-dark-300 focus:border-yellow-500 focus:text-yellow-500 focus:placeholder-yellow-300"
                  placeholder={dictionary.form.name.placeholder}
                  labelText={dictionary.form.name.label}
                  labelClassName="mb-2 block text-[12px]"
                  errorMessage={formState.errors.name?.message}
                  disabled={isSendingLoading}
                />
                <Input
                  name="email"
                  type="email"
                  control={control}
                  className="block w-full rounded-md border border-dark-100 bg-transparent px-5 py-4 text-[16px] text-white placeholder-dark-300 focus:border-yellow-500 focus:text-yellow-500 focus:placeholder-yellow-300"
                  placeholder={dictionary.form.email.placeholder}
                  labelText={dictionary.form.email.label}
                  labelClassName="mb-2 block text-[12px]"
                  errorMessage={formState.errors.name?.message}
                  disabled={isSendingLoading}
                />
              </div>
              <div className="w-full lg:col-span-2">
                <TextArea
                  placeholder={dictionary.form.message.placeholder}
                  name="message"
                  rows={5}
                  className="block h-auto w-full rounded-md border border-dark-100 bg-transparent px-5 py-4 text-[16px] text-white placeholder-dark-300 focus:border-yellow-500 focus:text-yellow-500 focus:placeholder-yellow-300"
                  control={control}
                  labelText={dictionary.form.message.label}
                  labelClassName="mb-3 text-dark-100 focus-within:text-yellow-500"
                />
              </div>
            </div>
            <div className="flex items-center justify-end pt-6">
              <Button name={dictionary.button_send} type="submit" disabled={isSendingLoading} />
            </div>
          </form>
        </div>
      </div>
    </>
  );
}

export default ContactView;

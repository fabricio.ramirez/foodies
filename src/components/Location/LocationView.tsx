'use client';

import Search from '@components/Icons/Search';
import React, {useState} from 'react';
import {Location, SelectedLocation} from '@entities/location';
import Title from '@components/Title/Title';
import cx from 'classnames';
import Pick from '@components/Icons/Pick';
import Delivery from '@components/Icons/Delivery';
import Image from 'next/image';
import GoogleMapView from '@components/Map/GoogleMap';
import {Translations} from 'translations';
import LocationCard from '@components/Location/Location';
import useLocations from '../../hooks/useLocations';

interface Props {
  dictionary: Translations['home']['locations'];
}

interface HandleProps {
  currentTarget: {
    id: string;
    classList: {
      add: (arg0: string) => void;
    };
  };
}

function LocationView({dictionary}: Props) {
  const {locations, setLocationSearchQuery, setType} = useLocations();
  const [selectedLocation, setSelectedLocation] = useState<SelectedLocation | null>(null);

  const handleSelection = (e: HandleProps) => {
    document.querySelectorAll(`button.location-btn`).forEach(item => {
      item.classList.remove('bg-black');
      item.classList.remove('text-white');
    });

    e.currentTarget.classList.add('bg-black');
    e.currentTarget.classList.add('text-white');
    setType(e.currentTarget.id);
  };

  const handleSearch = (event: {currentTarget: {value: string}}) => {
    setLocationSearchQuery(event.currentTarget.value);
  };

  const handleSelectedLocation = (data: Location) => {
    const selection = {
      id: data.id,
      name: data.title,
      position: {
        lat: data.latitude ?? 0,
        lng: data.longitude ?? 0,
      },
    };
    setSelectedLocation(selection);
  };

  return (
    <div className="flex flex-col lg:grid lg:grid-cols-11" data-cy="locationView">
      <div className="col-span-5 py-5">
        <div className="flex w-full items-center justify-center px-4 pb-6 md:px-[5vw]">
          <div className="flex w-full">
            <Title
              name={dictionary.title}
              className={cx(
                'text-left font-druk text-[40px] leading-[35px] md:text-[40px] lg:text-[40px]'
              )}
            />
          </div>
        </div>
        <div className="grid grid-cols-2">
          <div className="col-span-1">
            <button
              type="button"
              id="takeout"
              onClick={handleSelection}
              className={cx(
                'inline-flex w-full items-center justify-center gap-4 text-center md:gap-9 lg:gap-9',
                'font-syne text-[16px] font-bold md:text-[25px] lg:text-[25px] ',
                'bg-black py-[13.25px] text-white',
                'location-btn border-[1px] border-dark-200 dark:border-dark-400'
              )}
            >
              <Pick />
              <span>{dictionary.pick_title}</span>
            </button>
          </div>
          <div className="col-span-1">
            <button
              type="button"
              id="delivery"
              onClick={handleSelection}
              className={cx(
                'inline-flex w-full items-center justify-center gap-4 text-center md:gap-9 lg:gap-9',
                'font-syne text-[16px] font-bold md:text-[25px] lg:text-[25px] ',
                'location-btn border-[1px] border-dark-200 py-[13.25px] dark:border-dark-400'
              )}
            >
              <Delivery />
              <span>{dictionary.delivery_title}</span>
            </button>
          </div>
        </div>
        <div className="flex w-full items-center justify-center border-[1px] border-dark-200 focus-within:border-dark-900  dark:border-dark-400">
          <div className="inline-flex w-full items-center justify-center pl-4 md:pl-16 lg:pl-16">
            <span className="pr-8">
              <Search />
            </span>
            <input
              onChange={handleSearch}
              type="text"
              name="search"
              id="search-location"
              className="block w-full bg-transparent py-[12.75px] text-left text-[18px] leading-[24.51px] focus:outline-0 dark:placeholder-dark-300"
              placeholder={dictionary.search_placeholder}
            />
          </div>
        </div>
        <div
          className={`${
            locations && locations.data.length > 0 ? 'hidden' : null
          } flex w-full flex-col items-center justify-center py-16`}
        >
          <div className="flex w-full items-center justify-center">
            <Image
              src="/img/error_search_location.png"
              alt="Error Location"
              width={216}
              height={160}
              className="min-w-[216px]"
            />
          </div>
          <div className="w-1/2 py-8 text-center">
            <h1 className="font-syne text-[20px] font-bold">{dictionary.error}</h1>
          </div>
        </div>
        <div
          className={`flex w-full flex-col gap-3 px-4 py-4 md:px-[5vw] ${
            !locations?.data ? 'hidden' : null
          }`}
        >
          {locations?.data.map((item: Location) => (
            <LocationCard
              isSelected={selectedLocation?.id === item.id}
              key={item.id}
              location={item}
              handleSelectedLocation={handleSelectedLocation}
            />
          ))}
        </div>
      </div>
      <div className="col-span-6 hidden md:block lg:block">
        <GoogleMapView selectedLocation={selectedLocation} />
      </div>
    </div>
  );
}

export default LocationView;

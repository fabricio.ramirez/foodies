import React from 'react';
import LocationCard from './Location';
import style from './Locations.module.css';

describe('<LocationCard />', () => {
  it('display the correct information passed', () => {
    const location = {
      id: '1',
      description: 'This is a test',
      url: 'https://goo.gl/maps/7edUq69frk5mQFEY7',
      type: 'pick',
      title: 'This is a test Title',
    };
    // see: https://on.cypress.io/mounting-react
    cy.mount(
      <LocationCard isSelected={false} location={location} handleSelectedLocation={() => {}} />
    );

    cy.get('[data-cy="locationCard"] [data-location="title"]').should('have.text', location.title);
    cy.get('[data-cy="locationCard"] [data-location="description"]').should(
      'have.text',
      location.description
    );
    cy.get('[data-cy="locationCard"]').should('have.class', style.notActive);
  });

  it('display the correct information passed and it is highlight on active', () => {
    const location = {
      id: '1',
      description: 'This is a test',
      url: 'https://goo.gl/maps/7edUq69frk5mQFEY7',
      type: 'pick',
      title: 'This is a test Title',
    };
    // see: https://on.cypress.io/mounting-react
    cy.mount(<LocationCard isSelected location={location} handleSelectedLocation={() => {}} />);

    cy.get('[data-cy="locationCard"] [data-location="title"]').should('have.text', location.title);
    cy.get('[data-cy="locationCard"] [data-location="description"]').should(
      'have.text',
      location.description
    );
    cy.get('[data-cy="locationCard"]').should('have.class', style.active);
  });
});

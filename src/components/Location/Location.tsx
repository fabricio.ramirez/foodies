import React from 'react';
import {Location} from '@entities/location';
import useGetUrl from '../../hooks/useGetUrl';
import style from './Locations.module.css';

interface Props {
  location: Location;
  handleSelectedLocation: (data: Location) => void;
  isSelected: boolean;
}

function LocationCard({location, handleSelectedLocation, isSelected}: Props) {
  const {latitude, longitude} = useGetUrl({link: location.url});

  return (
    <button
      data-cy="locationCard"
      type="button"
      onClick={() => {
        const selectedLocation: Location = {
          ...location,
          latitude: parseFloat(latitude),
          longitude: parseFloat(longitude),
        };
        handleSelectedLocation(selectedLocation);
      }}
      className={`${isSelected ? style.active : style.notActive} dark:border-dark-200`}
    >
      <div className="flex flex-col items-center justify-start px-6 py-4 text-left">
        <div className="w-full pt-1">
          <h1 className="font-syne text-[20px] font-bold leading-[24px]" data-location="title">
            {location.title}
          </h1>
        </div>
        <div className="w-full pt-1">
          {location.description.includes('p.m') ? (
            <>
              <p className="text-[16px] leading-[21.79px]">
                {`${location.description.split('p.m. ')[0]} p.m. `}
              </p>
              <p className="text-[16px] leading-[21.79px]">
                {location?.description.split('p.m. ')[1]}
              </p>
            </>
          ) : (
            <p className="text-[16px] leading-[21.79px]" data-location="description">
              {location.description}
            </p>
          )}
        </div>
      </div>
    </button>
  );
}

export default LocationCard;

import 'server-only';

// @ts-ignore
import {Translations} from 'translations';

const dictionaries = {
  en: (path: string) => import(`/public/translate/${path}/en.json`).then(m => m.default),
  es: (path: string) => import(`/public/translate/${path}/es.json`).then(m => m.default),
};

const getTranslation = async <T extends keyof Translations>(
  path: T,
  lang: keyof typeof dictionaries
  // @ts-ignore
): Promise<Translations[T]> => dictionaries[lang](path);

export default getTranslation;

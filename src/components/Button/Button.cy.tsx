import React from 'react';
import Button from './Button';

describe('<Button />', () => {
  it('renders', () => {
    cy.mount(<Button name="test" />);
  });

  it('display the correct name given in props', () => {
    const name = 'test';
    cy.mount(<Button name={name} />);

    cy.get('[data-cy="button"]').should('have.text', name);
  });

  it('don`t display a name when no value is sent', () => {
    // @ts-ignore
    cy.mount(<Button />);

    cy.get('[data-cy="button"]').should('not.have.text');
  });

  it('trigger position aware animation on hover', () => {
    const name = 'test';
    cy.mount(<Button name={name} />);

    cy.get('[data-cy="button"]').trigger('mouseover');
    cy.get('[data-cy="button"] span').should('have.class', 'in_progress');
  });
});

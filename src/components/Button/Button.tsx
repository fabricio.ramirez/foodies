'use client';

import React from 'react';
import style from './Button.module.css';

type ButtonTypes = 'button' | 'submit' | 'reset' | undefined;

interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  name: string;
  type?: ButtonTypes;
}

function Button({name, type}: Props) {
  return (
    /* eslint-disable react/button-has-type */
    <button
      data-cy="button"
      type={type}
      onMouseEnter={e => {
        const relX = e.clientX - e.currentTarget.getBoundingClientRect().left;
        const relY = e.clientY - e.currentTarget.getBoundingClientRect().top;
        const span = e.currentTarget.querySelector('span');
        // @ts-ignore
        span.style.top = `${relY}px` ?? null;
        // @ts-ignore
        span.style.left = `${relX}px` ?? null;
        span?.classList.add('in_progress');
      }}
      onMouseLeave={e => {
        const span = e.currentTarget.querySelector('span');
        span?.classList.replace('in_progress', 'out');
      }}
      className={`${style.btnPositionAware} z-10`}
    >
      <p className="relative z-10">{name}</p>
      <span className="-z-1" />
    </button>
  );
}

const defaultProps = {
  type: 'button',
};

Button.defaultProps = defaultProps;

export default Button;

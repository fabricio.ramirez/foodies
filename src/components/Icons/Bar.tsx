import React from 'react';

interface Props {
  height?: number;
  width?: number;
  className?: string;
}

function Bar({height, width, className}: Props) {
  return (
    <svg
      data-cy="icon"
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 0 413 60"
      fill="none"
      className={className}
    >
      <rect
        x="0.710449"
        y="5.30322"
        width={width}
        height={height}
        transform="rotate(-0.641723 0.710449 5.30322)"
        fill="#FFD600"
      />
    </svg>
  );
}

const defaultProps = {
  height: '',
  width: '',
  className: '',
};

Bar.defaultProps = defaultProps;

export default Bar;

import React from 'react';
import Bar from './Bar';

describe('<Bar />', () => {
  it('Use correct props data', () => {
    const className = 'test';

    cy.mount(<Bar className={className} />);

    cy.get('[data-cy="icon"]').should('have.class', className);
  });
});

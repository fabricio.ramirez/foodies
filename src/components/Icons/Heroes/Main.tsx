import React from 'react';

interface Props {
  height?: number;
  width?: number;
  className?: string;
}

function MainHero({height, width, className}: Props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width ?? '792'}
      height={height ?? '794'}
      viewBox="0 0 792 794"
      fill="none"
      className={className}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M246.026 -218C147.187 -140 3.11211 16.4343 181.914 102C376.252 195 251.035 360 159.876 414C66.7656 469.157 16.6274 523.667 6.60995 565C-26.1136 714.333 49.6849 946 530.52 650C650.168 576.345 734.999 531.709 793.378 508.112V-40H807V-228H246.026V-218Z"
        fill="#FFD600"
      />
    </svg>
  );
}

const defaultProps = {
  height: '794',
  width: '792',
  className: '',
};

MainHero.defaultProps = defaultProps;

export default MainHero;

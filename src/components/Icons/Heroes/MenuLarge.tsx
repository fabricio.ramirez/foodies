import React from 'react';

interface Props {
  height?: number;
  width?: number;
  className?: string;
}

function MenuLarge({height, width, className}: Props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox="0 0 605 610"
      fill="none"
      className={className}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M713.185 -39.8995H748.529V622L1021.43 622L1021.43 61.0259L1011.43 61.0259C939.652 -29.9278 801.455 -159.189 713.185 -39.8995ZM606.667 634.035C383.809 838.927 205.581 727.269 143.43 644.038C266.763 744.88 439.43 826.355 143.43 345.52C-152.57 -135.315 79.0963 -211.114 228.43 -178.39C269.763 -168.373 324.273 -118.234 379.43 -25.1236C419.262 42.1183 519.491 127.892 606.667 90.2722V634.035Z"
        fill="#FFD600"
      />
    </svg>
  );
}

const defaultProps = {
  height: '610',
  width: '605',
  className: '',
};

MenuLarge.defaultProps = defaultProps;

export default MenuLarge;

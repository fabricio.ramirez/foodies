import React from 'react';

interface Props {
  height?: number;
  width?: number;
  className?: string;
}

function MenuSmall({height, width, className}: Props) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={width}
      height={height}
      viewBox="0 0 226 366"
      fill="none"
      className={className}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M589.173 -34.8943C543.719 -92.3856 452.559 -176.19 402.696 -72.1859C348.501 40.8541 252.35 -31.9808 220.882 -85.0048C188.74 -139.164 156.975 -168.328 132.888 -174.155C45.8658 -193.189 -89.1354 -149.1 83.3553 130.587C255.846 410.274 155.226 362.883 83.3553 304.226C120.262 353.56 227.408 420.063 360.739 291.407H461.328V-29.3182H551.638V291.407H595V-34.8943H589.173Z"
        fill="#FFD600"
      />
    </svg>
  );
}

const defaultProps = {
  height: '366',
  width: '226',
  className: '',
};

MenuSmall.defaultProps = defaultProps;

export default MenuSmall;

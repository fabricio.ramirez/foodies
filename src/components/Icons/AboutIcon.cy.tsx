import React from 'react';
import AboutIcon from './AboutIcon';

describe('<AboutIcon />', () => {
  it('Use correct props data', () => {
    const height = 48;
    const width = 25;
    const className = 'test';

    cy.mount(<AboutIcon className={className} width={width} height={height} />);

    cy.get('[data-cy="icon"]').should('have.class', className);
    cy.get('[data-cy="icon"]').should('have.css', 'height', `${height}px`);
    cy.get('[data-cy="icon"]').should('have.css', 'width', `${width}px`);
  });

  it('Use correct default props data', () => {
    const className = 'test';

    cy.mount(<AboutIcon />);

    cy.get('[data-cy="icon"]').should('not.have.class', className);
    cy.get('[data-cy="icon"]').should('have.css', 'height', `352px`);
    cy.get('[data-cy="icon"]').should('have.css', 'width', `830px`);
  });
});

import React from 'react';
import DoubleArrows from './DoubleArrows';

describe('<DoubleArrows />', () => {
  it('Use correct props data', () => {
    const height = 48;
    const width = 25;
    const className = 'test';

    cy.mount(<DoubleArrows className={className} width={width} height={height} />);

    cy.get('[data-cy="icon"]').should('have.class', className);
    cy.get('[data-cy="icon"]').should('have.css', 'height', `${height}px`);
    cy.get('[data-cy="icon"]').should('have.css', 'width', `${width}px`);
  });

  it('Use correct default props data', () => {
    const className = 'test';

    cy.mount(<DoubleArrows />);

    cy.get('[data-cy="icon"]').should('not.have.class', className);
    cy.get('[data-cy="icon"]').should('have.css', 'height', `248px`);
    cy.get('[data-cy="icon"]').should('have.css', 'width', `524px`);
  });
});

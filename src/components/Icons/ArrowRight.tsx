import React from 'react';

interface Props {
  height?: number;
  width?: number;
  className?: string;
}

function ArrowRight({height, width, className}: Props) {
  return (
    <svg
      data-cy="icon"
      xmlns="http://www.w3.org/2000/svg"
      width={width ?? '24'}
      height={height ?? '24'}
      viewBox="0 0 24 24"
      fill="none"
      className={className}
    >
      <path
        d="M5 12H19"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12 5L19 12L12 19"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

const defaultProps = {
  height: '24',
  width: '24',
  className: '',
};

ArrowRight.defaultProps = defaultProps;

export default ArrowRight;

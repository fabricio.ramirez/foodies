'use client';

import React, {useEffect} from 'react';
// @ts-ignore
import ScrollOut from 'scroll-out';

// @ts-ignore
import Splitting from 'splitting';

interface Props {
  name: string;
  className?: string;
  name2?: string;
  className2?: string;
}

function Title({name, className, name2, className2}: Props) {
  const words = name.split(' ');

  const words2 = name2?.split(' ') ?? false;

  useEffect(() => {
    Splitting();
    ScrollOut();
  }, []);

  return (
    <span className={`title-animation ${className}`}>
      {words.map((word: string) => (
        <div key={word} data-scroll="out" data-splitting="" className="folding inline-block">
          {word}{' '}
        </div>
      ))}
      {words2
        ? words2.map((word: string) => (
            <div
              key={word}
              data-scroll="out"
              data-splitting=""
              className={`folding inline-block ${className2}`}
            >
              {word}{' '}
            </div>
          ))
        : null}
    </span>
  );
}

const defaultProps = {
  className: '',
  name2: '',
  className2: '',
};

Title.defaultProps = defaultProps;

export default Title;

'use client';

import React, {useEffect} from 'react';
// @ts-ignore
import ScrollOut from 'scroll-out';
// @ts-ignore
import Splitting from 'splitting';

interface Props {
  text: string;
  className?: string;
}

function Description({text, className}: Props) {
  const words = text.split(' ');

  useEffect(() => {
    Splitting();
    ScrollOut();
  }, []);

  return (
    <div className={`desc-animation ${className}`} data-cy="description" data-text={text}>
      {words.map((word: string) => (
        <div key={`${word}-${Math.random()}`} className="folding">
          <div
            data-scroll="out"
            data-splitting=""
            className="pre-word inline-block"
            style={{['--word-index' as any]: words.indexOf(word)}}
          >
            {word}{' '}
          </div>
        </div>
      ))}
    </div>
  );
}

const defaultProps = {
  className: '',
};

Description.defaultProps = defaultProps;

export default Description;

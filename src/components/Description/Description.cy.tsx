import React from 'react';
import Description from './Description';

describe('<Description />', () => {
  it('display text given in props', () => {
    const text = 'This is a test';
    cy.mount(<Description text={text} />);

    cy.get('[data-cy="description"]').should('have.attr', 'data-text', text);
  });

  it('display class given in props', () => {
    const classText = 'Testing';
    // see: https://on.cypress.io/mounting-react
    cy.mount(<Description text="test" className={classText} />);

    cy.get('[data-cy="description"]').should('have.class', classText);
  });
});

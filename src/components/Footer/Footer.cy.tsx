import React from 'react';
import Footer from './Footer';

describe('<Footer />', () => {
  const dictionary = {
    footer: {
      links: {
        locations: 'test',
        about: 'about test',
        testimonials: '¿es esto un test?',
        contact: 'Test 1212',
      },
    },
  };
  it('it display correct information given in dictionary', () => {
    // @ts-ignore
    cy.mount(<Footer dictionary={dictionary} />);

    cy.get('[data-cy="footer"] [data-footer="link_about"]').should(
      'have.text',
      dictionary.footer.links.about
    );
    cy.get('[data-cy="footer"] [data-footer="link_locations"]').should(
      'have.text',
      dictionary.footer.links.locations
    );
    cy.get('[data-cy="footer"] [data-footer="link_testimonials"]').should(
      'have.text',
      dictionary.footer.links.testimonials
    );
    cy.get('[data-cy="footer"] [data-footer="link_contact"]').should(
      'have.text',
      dictionary.footer.links.contact
    );
  });
});

import React from 'react';
import Image from 'next/image';
// @ts-ignore
import {Translations} from 'translations';
import Link from 'next/link';

interface Props {
  dictionary: Translations['home'];
}

function Footer({dictionary}: Props) {
  return (
    <footer className="flex w-full flex-col items-center justify-center py-8" data-cy="footer">
      <div className="flex w-full max-w-[95vw] items-center justify-center border-b-[1px] border-yellow-500 lg:max-w-[90vw]">
        <div className="flex w-full max-w-[85vw] flex-col text-center md:flex-row md:text-left lg:max-w-[80vw] lg:flex-row lg:text-left">
          <div className="w-full py-6">
            <h1 className="font-druk text-[27px] font-bold text-dark-300">Foodies</h1>
          </div>
          <div className="flex w-full justify-center gap-2 md:justify-end lg:justify-end">
            <div className="pb-8">
              <Image
                src="/img/app_store.png"
                width={184}
                height={54}
                className="max-w-[120px] rounded-md md:max-w-[184px] lg:max-w-[184px]"
                alt="App Store"
              />
            </div>
            <div className="pb-8">
              <Image
                src="/img/play.png"
                width={184}
                height={54}
                className="max-w-[120px] rounded-md md:max-w-[184px] lg:max-w-[184px]"
                alt="App Store"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="flex w-full max-w-[95vw] items-center justify-center lg:max-w-[90vw]">
        <div className="flex w-full max-w-[85vw] flex-col justify-start gap-0 pt-4 text-center md:flex-row md:gap-10 md:text-left lg:max-w-[80vw] lg:flex-row lg:gap-10 lg:text-left">
          <div className="py-2 md:py-6 lg:py-6">
            <Link
              className="text-[16px] text-dark-300"
              data-footer="link_locations"
              href="#restaurants"
            >
              {dictionary.footer.links.locations}
            </Link>
          </div>
          <div className="py-2 md:py-6 lg:py-6">
            <Link className="text-[16px] text-dark-300" data-footer="link_about" href="#about">
              {dictionary.footer.links.about}
            </Link>
          </div>
          <div className="py-2 md:py-6 lg:py-6">
            <Link
              className="text-[16px] text-dark-300"
              data-footer="link_testimonials"
              href="#testimonials"
            >
              {dictionary.footer.links.testimonials}
            </Link>
          </div>
          <div className="py-2 md:py-6 lg:py-6">
            <Link className="text-[16px] text-dark-300" data-footer="link_contact" href="#contact">
              {dictionary.footer.links.contact}
            </Link>
          </div>
        </div>
      </div>
    </footer>
  );
}

export default Footer;

'use client';

import React, {useEffect, useState} from 'react';
// @ts-ignore
import {Translations} from 'translations';
import {usePathname} from 'next/navigation';
import Link from 'next/link';
import cx from 'classnames';
import NavbarItem from '@components/Navbar/NavbarItem';
import DarkSwitch from '@components/DarkMode/DarkSwitch';

export const routes = (dictionary: Translations['home']) => [
  {
    name: dictionary.items.about,
    path: '#about',
  },
  {
    name: dictionary.items.restaurants,
    path: '#restaurants',
  },
  {
    name: dictionary.items.menu,
    path: '/menu',
  },
  {
    name: dictionary.items.contact,
    path: '#contact',
  },
];

interface Props {
  dictionary: Translations['home'];
}

export function Navbar({dictionary}: Props) {
  const [isScrolled, setIsScrolled] = useState(false);

  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => {
    setIsOpen(!isOpen);
  };

  const handleScroll = () => {
    setIsScrolled(window.scrollY > 10);
  };

  useEffect(() => {
    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  });

  const mainPathName = usePathname();

  const [pathName, setPathName] = useState('');

  const handlePathChange = (path: string) => {
    setPathName(path);
  };

  useEffect(() => {
    setPathName(`#${window.location.href.split('#')[1]}` ?? '');
  }, []);

  return (
    <header
      className={cx(
        'fixed inset-x-0 top-0 z-50 flex w-full flex-wrap md:flex-nowrap md:justify-start',
        `${
          isScrolled
            ? 'bg-white bg-opacity-95 text-black transition duration-700 ease-in dark:bg-dark-700 dark:bg-opacity-95 dark:text-white'
            : 'bg-transparent transition duration-300 ease-in dark:text-white'
        }`,
        'py-8 text-sm md:py-8 lg:py-10',
        `${isOpen && mainPathName === '/menu' ? 'bg-dark-700' : 'bg-transparent'}`,
        `${isOpen ? 'bg-white dark:bg-dark-700' : 'bg-transparent'}`,
        // eslint-disable-next no-nested-ternary
        `${mainPathName === '/menu' && !isScrolled ? 'text-white' : 'text-black'}`
      )}
    >
      <nav
        className="relative mx-auto w-full max-w-[95vw] px-4 md:flex md:flex-col md:items-start md:justify-between lg:max-w-[90vw] lg:flex-row"
        aria-label="Global"
      >
        <div className="flex items-center justify-between sm:w-full md:w-full lg:w-auto">
          <Link
            onClick={() => {
              handlePathChange('');
            }}
            className="flex-none pr-[60px] font-druk text-[27px] font-bold"
            href="/home"
          >
            Foodies
          </Link>
          <div className="lg:hidden">
            <button
              onClick={toggle}
              type="button"
              className={`${
                isScrolled ? 'text-black dark:text-white' : 'text-black'
              } hs-collapse-toggle inline-flex items-center justify-center text-sm transition-all`}
              data-hs-collapse="#navbar-collapse-with-animation"
              aria-controls="navbar-collapse-with-animation"
              aria-label="Toggle navigation"
            >
              <svg
                className={cx(
                  isScrolled ? 'dark:text-white' : '',
                  !isOpen ? 'block text-black' : 'hidden'
                )}
                width="40"
                height="40"
                fill="currentColor"
                viewBox="0 0 16 16"
              >
                <path
                  fillRule="evenodd"
                  d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"
                />
              </svg>
              <svg
                className={cx(
                  isOpen ? 'block text-black dark:text-white' : 'hidden',
                  isOpen && mainPathName === '/menu' ? 'text-white' : ''
                )}
                width="40"
                height="40"
                fill="currentColor"
                viewBox="0 0 16 16"
              >
                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z" />
              </svg>
            </button>
          </div>
        </div>
        <div
          className={`${
            isOpen ? 'block' : 'hidden'
          } flex grow basis-full flex-col overflow-hidden transition-all duration-300 lg:flex lg:flex-row`}
        >
          {routes(dictionary).map(navItem => (
            <NavbarItem
              key={navItem.path}
              isActive={mainPathName === navItem.path || pathName === navItem.path}
              path={navItem.path}
              name={navItem.name}
              handlePathChange={handlePathChange}
            />
          ))}
        </div>
        <DarkSwitch />
      </nav>
    </header>
  );
}

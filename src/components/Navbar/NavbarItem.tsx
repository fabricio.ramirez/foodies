import Link from 'next/link';

import {usePathname, useRouter} from 'next/navigation';
import style from './NavbarItem.module.css';

interface Props {
  path: string;
  name: string;
  isActive: boolean;
  handlePathChange: (path: string) => void;
}

function NavbarItem({name, path, isActive, handlePathChange}: Props) {
  const mainPathName = usePathname();

  const router = useRouter();

  const handleScroll = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    e.preventDefault();

    const {href} = e.currentTarget;
    const targetId = href.split('#')[1];

    const elem = document.getElementById(targetId);

    elem?.scrollIntoView({
      behavior: 'smooth',
    });
  };

  const handleClick = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
    e.preventDefault();

    document.querySelectorAll(`a.${style.link}`).forEach(item => {
      item.classList.remove(style['active-link']);
    });

    if (mainPathName.includes('home') && path.includes('#')) {
      handleScroll(e);
      handlePathChange(path);
    } else if (!path.includes('menu')) {
      router.push(`/home${path}`);
    } else {
      router.push(path);
    }
  };

  return (
    <Link
      onClick={e => {
        handleClick(e);
      }}
      className={`${style.link} ${isActive && style['active-link']} `}
      href={path}
    >
      {name}
    </Link>
  );
}

export default NavbarItem;

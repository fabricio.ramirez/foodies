import React from 'react';
import {Category} from '@entities/dish';
import CategoryView from './Categories';
import style from './Category.module.css';

describe('<CategoryView />', () => {
  it('display correct props data', () => {
    const categories: Category[] = [
      {
        id: '1',
        name: 'Test_1',
        slug: 'test_1',
      },
      {
        id: '2',
        name: 'Test_2',
        slug: 'test_2',
      },
    ];

    cy.viewport(1000, 1000);

    cy.mount(<CategoryView categories={categories} handleSelection={() => {}} />);
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(1) button').should(
      'have.attr',
      'data-id',
      'default'
    );
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(1) button').should(
      'have.class',
      style['active-link']
    );
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(2) button').should(
      'have.attr',
      'data-id',
      categories[0].id
    );
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(2) button').should(
      'not.have.class',
      style['active-link']
    );
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(3) button').should(
      'have.attr',
      'data-id',
      categories[1].id
    );
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(3) button').should(
      'not.have.class',
      style['active-link']
    );
  });

  it('display correct selected option', () => {
    const categories: Category[] = [
      {
        id: '1',
        name: 'Test_1',
        slug: 'test_1',
      },
      {
        id: '2',
        name: 'Test_2',
        slug: 'test_2',
      },
    ];

    cy.viewport(1000, 1000);

    cy.mount(<CategoryView categories={categories} handleSelection={() => {}} />);
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(1) button').should(
      'have.attr',
      'data-id',
      'default'
    );
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(1) button').should(
      'have.class',
      style['active-link']
    );
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(2) button').should(
      'have.attr',
      'data-id',
      categories[0].id
    );
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(2) button').should(
      'not.have.class',
      style['active-link']
    );
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(3) button').should(
      'have.attr',
      'data-id',
      categories[1].id
    );
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(3) button').should(
      'not.have.class',
      style['active-link']
    );

    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(3) button').trigger(
      'click'
    );
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(1) button').should(
      'not.have.class',
      style['active-link']
    );
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(2) button').should(
      'not.have.class',
      style['active-link']
    );
    cy.get('[data-cy="category"] [data-category="buttons"] div:nth-child(3) button').should(
      'have.class',
      style['active-link']
    );
  });
});

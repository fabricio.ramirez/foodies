'use client';

import Search from '@components/Icons/Search';
import React, {useState} from 'react';
import CategoryView from '@components/Menu/Categories';
import {Dish} from '@entities/dish';
import DishView from '@components/Menu/Dish';
import Pagination from '@components/Menu/Pagination';
import Image from 'next/image';
import useCategories from '../../hooks/useCategories';
import useDishes from '../../hooks/useDishes';

function MenuView() {
  const {categories} = useCategories();
  const [isOpen, setIsOpen] = useState(false);

  const {dishes, setDishSearchQuery, setSelectedCategories, setPageNumber} = useDishes();

  const handleSelection = (items: string[]) => {
    setSelectedCategories(items ?? null);
  };

  const handlePagination = (page: number) => {
    setPageNumber(page);
  };

  const handleSearch = (event: {currentTarget: {value: string}}) => {
    setDishSearchQuery(event.currentTarget.value);
  };

  return (
    <div className="mx-6 flex flex-col items-center justify-center md:mx-8 lg:mx-[5vw]">
      <div className="w-full py-6 pb-12 md:py-8 lg:py-20">
        <div className="md:flex md:flex-col lg:grid lg:grid-cols-3">
          <div className="flex flex-row items-center justify-between md:flex md:justify-center lg:col-span-1 lg:w-full">
            <div className="flex w-10/12 items-center justify-center rounded-md border-[1px] border-dark-300 focus-within:border-dark-900 dark:border-dark-400 md:w-full md:max-w-[60vw]">
              <div className="inline-flex w-full items-center justify-center pl-4">
                <span className="pr-4 md:pr-8">
                  <Search />
                </span>
                <input
                  onChange={handleSearch}
                  type="text"
                  name="search"
                  id="search-location"
                  className="block w-full bg-transparent py-[12.75px] text-left leading-[24.51px] placeholder-dark-300 focus:outline-0 dark:placeholder-dark-400 md:text-[18px]"
                  placeholder="Busca tu platillo favorite..."
                />
              </div>
            </div>
            <button
              type="button"
              className="flex w-2/12 items-center justify-center px-2"
              onClick={() => {
                setIsOpen(!isOpen);
              }}
            >
              <svg
                className="block text-black dark:text-white md:hidden"
                width="24"
                height="20"
                viewBox="0 0 24 20"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M4 11C4.55228 11 5 11.4477 5 12V19C5 19.5523 4.55228 20 4 20C3.44772 20 3 19.5523 3 19V12C3 11.4477 3.44772 11 4 11Z"
                  fill="currentColor"
                />
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M4 0C4.55228 0 5 0.447715 5 1V8C5 8.55228 4.55228 9 4 9C3.44772 9 3 8.55228 3 8V1C3 0.447715 3.44772 0 4 0Z"
                  fill="currentColor"
                />
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M12 9C12.5523 9 13 9.44771 13 10V19C13 19.5523 12.5523 20 12 20C11.4477 20 11 19.5523 11 19V10C11 9.44771 11.4477 9 12 9Z"
                  fill="currentColor"
                />
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M12 0C12.5523 0 13 0.447715 13 1V6C13 6.55228 12.5523 7 12 7C11.4477 7 11 6.55228 11 6V1C11 0.447715 11.4477 0 12 0Z"
                  fill="currentColor"
                />
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M20 13C20.5523 13 21 13.4477 21 14V19C21 19.5523 20.5523 20 20 20C19.4477 20 19 19.5523 19 19V14C19 13.4477 19.4477 13 20 13Z"
                  fill="currentColor"
                />
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M20 0C20.5523 0 21 0.447715 21 1V10C21 10.5523 20.5523 11 20 11C19.4477 11 19 10.5523 19 10V1C19 0.447715 19.4477 0 20 0Z"
                  fill="currentColor"
                />
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M0 12C0 11.4477 0.447715 11 1 11H7C7.55228 11 8 11.4477 8 12C8 12.5523 7.55228 13 7 13H1C0.447715 13 0 12.5523 0 12Z"
                  fill="currentColor"
                />
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M8 6C8 5.44772 8.44771 5 9 5H15C15.5523 5 16 5.44772 16 6C16 6.55228 15.5523 7 15 7H9C8.44771 7 8 6.55228 8 6Z"
                  fill="currentColor"
                />
                <path
                  fillRule="evenodd"
                  clipRule="evenodd"
                  d="M16 14C16 13.4477 16.4477 13 17 13H23C23.5523 13 24 13.4477 24 14C24 14.5523 23.5523 15 23 15H17C16.4477 15 16 14.5523 16 14Z"
                  fill="currentColor"
                />
              </svg>
            </button>
          </div>
          <CategoryView
            handleSelection={handleSelection}
            categories={categories ?? []}
            isOpen={isOpen}
          />
        </div>
      </div>
      <div className="w-full">
        <div
          className={`${
            dishes && dishes.data.length > 0 ? 'hidden' : null
          } flex w-full flex-col items-center justify-center py-16 `}
        >
          <div className="flex w-full items-center justify-center">
            <Image
              src="/img/error_menu.png"
              alt="Error Location"
              width={216}
              height={160}
              className="min-w-[216px]"
            />
          </div>
          <div className="py-8 text-center lg:w-1/3">
            <h1 className="font-syne text-[24px] font-bold">¡Platillo no encontrado!</h1>
            <p className="py-6 text-[18px]">
              Te invitamos a que verifiques si el nombre esta bien escrito o prueba buscando un
              nuevo platillo.
            </p>
          </div>
        </div>
        <div className="grid gap-x-6 gap-y-12 md:grid-cols-2 lg:grid-cols-3 xl:grid-cols-4">
          {dishes?.data.map((item: Dish) => <DishView key={item.id} dish={item} />)}
        </div>
      </div>

      {dishes && dishes.data.length > 0 ? (
        <Pagination
          currentPage={dishes?.currentPage}
          lastPage={dishes?.lastPage}
          hasNextPage={dishes?.hasNextPage}
          handlePagination={handlePagination}
        />
      ) : null}
    </div>
  );
}

export default MenuView;

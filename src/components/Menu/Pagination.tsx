import React from 'react';

interface Props {
  lastPage?: number;
  hasNextPage?: boolean;
  currentPage?: number;
  handlePagination: (page: number) => void;
}

function Pagination({currentPage, lastPage, hasNextPage, handlePagination}: Props) {
  const pages = Array.from(Array(lastPage).keys(), n => n + 1);

  return (
    <div className="w-full pb-6 pt-12 md:py-8 lg:py-16">
      <div className="flex flex-wrap items-center justify-center gap-2">
        {currentPage && currentPage !== 1 ? (
          <div className="w-auto">
            <button
              type="button"
              onClick={() => {
                handlePagination(currentPage - 1);
              }}
              className="rounded-md bg-yellow-200 px-5 py-2 text-[20px] text-black hover:bg-yellow-500"
            >
              Anterior
            </button>
          </div>
        ) : null}
        {pages.map((item: number) => (
          <div key={item} className="w-auto">
            <button
              type="button"
              onClick={() => {
                handlePagination(item);
              }}
              className={`rounded-md ${
                item === currentPage
                  ? 'bg-black text-yellow-500'
                  : 'border-[1px] border-dark-300 text-black hover:bg-black hover:text-yellow-500 dark:bg-dark-400 dark:text-white hover:dark:bg-black dark:hover:text-yellow-500'
              }  px-5 py-2 text-[20px] font-bold`}
            >
              {item}
            </button>
          </div>
        ))}
        {hasNextPage && currentPage ? (
          <div className="w-auto">
            <button
              type="button"
              onClick={() => {
                handlePagination(currentPage + 1);
              }}
              className="rounded-md bg-yellow-200 px-5 py-2 text-[20px] text-black hover:bg-yellow-500"
            >
              Siguiente
            </button>
          </div>
        ) : null}
      </div>
    </div>
  );
}

const defaultProps = {
  lastPage: 4,
  currentPage: 1,
  hasNextPage: true,
};

Pagination.defaultProps = defaultProps;

export default Pagination;

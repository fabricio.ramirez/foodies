import Image from 'next/image';
import React from 'react';
import {Category, Dish} from '@entities/dish';

interface Props {
  dish: Dish;
}
function DishView({dish}: Props) {
  return (
    <div
      className="w-full cursor-pointer rounded-lg shadow-md hover:shadow-2xl dark:bg-gray-800 lg:col-span-1"
      data-cy="dish"
    >
      <div className="relative flex flex-col">
        <div className="w-full">
          <Image
            src={dish.image}
            alt="Product 0"
            width={330}
            height={264}
            className="h-[30vh] w-full rounded-t-lg object-cover md:h-[30vh] lg:h-[25vh]"
          />
        </div>
        <div className="flex min-h-[250px] flex-col px-4 pb-4 lg:min-h-[300px] xl:min-h-[240px]">
          <div className="py-4">
            <h1 className="font-syne text-[22px] font-bold">{dish.title}</h1>
          </div>
          <div className="lg:mr-16">
            <p className="text-[18px]">{dish.description}</p>
          </div>
        </div>
        <div className="absolute bottom-0 flex w-full justify-between p-4">
          <div>
            <span className="text-[18px] text-dark-400 dark:text-dark-300">
              {dish.categories.map((item: Category) => item.name).join(', ')}
            </span>
          </div>
          <div>
            <span className="rounded-md bg-yellow-500 px-3 py-1 font-druk text-[16px] font-bold dark:text-black">
              ${dish.price.toFixed(2)}
            </span>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DishView;

import React from 'react';
import {Category} from '@entities/dish';
import style from './Category.module.css';

interface HandleProps {
  currentTarget: {
    id: string;
    classList: {
      add: (arg0: string) => void;
    };
  };
}

interface Props {
  handleSelection: (items: string[]) => void;
  categories: Category[];
  isOpen?: boolean;
}

function CategoryView({handleSelection, categories, isOpen}: Props) {
  const handleCategorySelection = (e: HandleProps) => {
    if (e.currentTarget.id === 'all') {
      document.querySelectorAll(`button.${style.link}`).forEach(item => {
        item.classList.remove(style['active-link']);
      });
      e.currentTarget.classList.add(style['active-link']);
    } else {
      document.querySelectorAll(`button.${style.link}`).forEach(item => {
        if (item.id === 'all') {
          item.classList.remove(style['active-link']);
        }
      });
      e.currentTarget.classList.add(style['active-link']);
    }

    const items: string[] = [];

    document.querySelectorAll(`button.${style['active-link']}`).forEach(item => {
      if (item.id !== 'all') {
        items.push(item.id);
      }
    });

    handleSelection(items);
  };

  return (
    <div
      className={`${
        isOpen ? 'block pt-8' : 'hidden'
      } w-full md:block md:py-8 lg:col-span-2 lg:block lg:py-0`}
      data-cy="category"
    >
      <div
        className="flex h-full flex-col items-center justify-center gap-6 md:flex-row md:gap-8 lg:justify-start lg:gap-2 lg:px-6 xl:gap-6 xl:px-12 2xl:gap-12"
        data-category="buttons"
      >
        <div className="relative flex h-[20px] w-auto items-center overflow-hidden overflow-x-clip">
          <button
            data-id="default"
            type="button"
            id="all"
            data-slug="all"
            data-name="all"
            className={`${style.link} ${style['active-link']}`}
            onClick={handleCategorySelection}
          >
            Todos
          </button>
        </div>
        {categories?.map((item: Category) => (
          <div
            key={item.id}
            className="relative flex h-[20px] w-auto items-center overflow-y-hidden overflow-x-clip"
          >
            <button
              data-id={item.id}
              type="button"
              id={item.id}
              data-slug={item.slug}
              data-name={item.name}
              className={`${style.link}`}
              onClick={handleCategorySelection}
            >
              {item.name}
            </button>
          </div>
        ))}
      </div>
    </div>
  );
}

const defaultProps = {
  isOpen: false,
};

CategoryView.defaultProps = defaultProps;
export default CategoryView;

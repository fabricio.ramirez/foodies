import {useEffect, useState} from 'react';
import {getLocationDetail} from '@service/locations';
import useMutation from 'swr/mutation';

interface Props {
  link: string;
}
const useGetUrl = ({link}: Props) => {
  const [latitude, setLatitude] = useState('');
  const [longitude, setLongitude] = useState('');

  const {trigger: getLocationDetailTrigger} = useMutation(
    'get-location-details',
    getLocationDetail,
    {
      onSuccess: data => {
        if (data.includes('maps/search')) {
          const urlData = data.split('search/').pop().split('&gl')[0].split(',%2B');
          setLatitude(urlData[0]);
          setLongitude(urlData[1]);
        } else if (data.includes('maps/place/')) {
          const urlData = data.split('/@').pop().split('z/data')[0].split(',');
          setLatitude(urlData[0]);
          setLongitude(urlData[1]);
        }
      },
    }
  );

  useEffect(() => {
    if (link) {
      getLocationDetailTrigger({
        url: link,
      });
    }
  }, [getLocationDetailTrigger, link]);

  return {
    latitude,
    longitude,
  };
};

export default useGetUrl;

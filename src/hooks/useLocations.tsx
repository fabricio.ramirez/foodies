'use client';

import {useEffect, useState} from 'react';
import useMutation from 'swr/mutation';
import {useDebounce} from 'usehooks-ts';
import {getLocations} from '@service/locations';
import {Location} from '@entities/location';

const useLocations = () => {
  const [locations, setLocations] = useState<{
    currentPage: number;
    limit?: number;
    hasNextPage: boolean;
    lastPage?: number;
    data: Location[];
  } | null>(null);

  const [locationSearchQuery, setLocationSearchQuery] = useState('');
  const debouncedLocationSearchQuery = useDebounce(locationSearchQuery, 500);
  const [type, setType] = useState<string | null>(null);
  const [pageNumber, setPageNumber] = useState<number | null>(null);

  const {trigger: getLocationsTrigger} = useMutation('get-locations', getLocations, {
    onSuccess: data => {
      setLocations({
        currentPage: data.meta.current_page,
        hasNextPage: data.meta.current_page < data.meta.last_page,
        lastPage: data.meta.last_page,
        data: data.data,
      });
    },
  });

  useEffect(() => {
    getLocationsTrigger({
      q: debouncedLocationSearchQuery,
      page: pageNumber || 1,
      limit: 12,
      type: type || 'takeout',
    });
  }, [getLocationsTrigger, type, pageNumber, debouncedLocationSearchQuery]);

  return {
    locations,
    locationSearchQuery,
    setLocationSearchQuery,
    setType,
    setPageNumber,
  };
};

export default useLocations;

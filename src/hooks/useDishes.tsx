'use client';

import {useEffect, useState} from 'react';
import getDishes from '@service/dishes';
import useMutation from 'swr/mutation';
import {useDebounce} from 'usehooks-ts';
import {Dish} from '@entities/dish';

const useDishes = () => {
  const [dishes, setDishes] = useState<{
    currentPage: number;
    limit?: number;
    hasNextPage: boolean;
    lastPage?: number;
    data: Dish[];
  } | null>(null);

  const [dishSearchQuery, setDishSearchQuery] = useState('');
  const debouncedDishSearchQuery = useDebounce(dishSearchQuery, 500);
  const [selectedCategories, setSelectedCategories] = useState<string[] | null>(null);
  const [pageNumber, setPageNumber] = useState<number | null>(null);

  const {trigger: getDishesTrigger} = useMutation('get-dishes', getDishes, {
    onSuccess: data => {
      setDishes({
        currentPage: data.meta.current_page,
        hasNextPage: data.meta.current_page < data.meta.last_page,
        lastPage: data.meta.last_page,
        data: data.data,
      });
    },
  });

  useEffect(() => {
    getDishesTrigger({
      q: debouncedDishSearchQuery,
      page: pageNumber || 1,
      limit: 12,
      ...(selectedCategories && {categories: selectedCategories}),
    });
  }, [getDishesTrigger, selectedCategories, pageNumber, debouncedDishSearchQuery]);

  return {
    dishes,
    dishSearchQuery,
    setDishSearchQuery,
    setSelectedCategories,
    setPageNumber,
  };
};

export default useDishes;

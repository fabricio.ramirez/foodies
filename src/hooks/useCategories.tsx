'use client';

import {useEffect, useState} from 'react';
import useMutation from 'swr/mutation';
import {Category} from '@entities/dish';
import getCategories from '@service/categories';

const useCategories = () => {
  const [categories, setCategories] = useState<Category[] | null>(null);

  const {trigger: getCategoriesTrigger} = useMutation('get-categories', getCategories, {
    onSuccess: data => {
      setCategories(data.data);
    },
  });

  useEffect(() => {
    getCategoriesTrigger();
  }, [getCategoriesTrigger]);

  return {
    categories,
  };
};

export default useCategories;

import {defineConfig} from 'cypress';
import dotenv from 'dotenv';

export default defineConfig({
  component: {
    devServer: {
      framework: 'next',
      bundler: 'webpack',
    },
  },
});

import {JsonObject} from 'type-fest';

import home from '@public/translate/home/es.json';
import menu from '@public/translate/menu/es.json';

declare module 'translations' {
  export interface Translations {
    home: typeof home & JsonObject;
    menu: typeof menu & JsonObject;
  }
}

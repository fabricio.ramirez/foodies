/** @type {import('next').NextConfig} */

const withPlugins = require('next-compose-plugins');
const bundleAnalyzer = require('@next/bundle-analyzer')({
  enabled: process.env.ANALYZE === 'true',
});

const nextConfig = {
  output: 'standalone',
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'cdn.elaniin.com',
        pathname: '/trainee-program/projects/foodies/assets/images/**',
      },
    ],
  },
};

module.exports = withPlugins([bundleAnalyzer], nextConfig);

# Foodies

DEMO https://foodies.tukicoders.com/

## Prerequisites

You will need [Node.js](https://nodejs.org) version 18.0 or greater installed on your system.

## Setup

Get the code by either cloning this repository using git

```
git clone git@gitlab.com:fabricio.ramirez/foodies.git
```

Once downloaded, open the terminal in the project directory, and install dependencies with:

```
yarn install
```

To properly use the Locations and Menu integration please add the following

```bash
# In your .env file

NEXT_PUBLIC_API_URL=https://api.foodies.elaniin.dev
NEXT_PUBLIC_GOOGLE_MAPS_API_KEY={Add your Google Map API Key}

```

Then start the project with 

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.
